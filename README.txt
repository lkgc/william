Copy the code from extensions_custom.conf into your own file in /etc/asterisk.

Copy the audio files into /var/lib/asterisk/sounds/en/William/

Create a Custom Destination in FreePBX with the following:

Target = William,talk,1
Description = William
Notes = blank
Return = Notes

Then point a number at that custom destination and you should
be good to talk to Lenny!